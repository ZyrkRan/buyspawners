package me.zyrkran.buyspawners.utils;

public class Messages {
	
	public static String NO_PERMISSION = "null";
	public static String NO_PERMISSION_TO_BREAK_SPAWNER = "null";
	public static String NO_PERMISSION_TO_PLACE_SPAWNER = "null";
	public static String MUST_BE_INGAME_PLAYER = "null";
	public static String CONFIG_RELOADED = "null";
	
	/* Player messages */
	public static String PLAYER_CHANGED_SPAWNER_TYPE = "null";
	public static String PLAYER_RECEIVED_SPAWNER = "null";	
	public static String PLAYER_PURCHASED_SPAWNER = "null";
	public static String PLAYER_PLACED_SPAWNER = "null";
	public static String PLAYER_DESTROYED_SPAWNER = "null";
	public static String PLAYER_OPENED_GUI = "null";
	
	/* Error messages */
	public static String INVALID_SPAWNER_TYPE = "null";
	public static String INVALID_PRICE = "null";
	public static String INVALID_PRICE_SYMBOL = "null";
	public static String INSUFFICIENT_MONEY = "null";
	public static String NOT_HOLDING_A_MOB_SPAWNER = "null";
	public static String NOT_LOOKING_AT_MOB_SPAWNER = "null";
	
	
	

}
