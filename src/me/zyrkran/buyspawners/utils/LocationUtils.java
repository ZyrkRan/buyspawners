package me.zyrkran.buyspawners.utils;

import org.bukkit.Bukkit;
import org.bukkit.Location;

public class LocationUtils {
	
	public static String stringFromLocation(Location loc){
		return loc.getWorld().getName() + ";" + loc.getX() + ";" + loc.getY() + ";" + loc.getZ();
	}
	
	public static Location locationFromString(String string){
		String[] a = string.split(";");
		Location location = new Location(
				Bukkit.getWorld(a[0]), 
				Double.parseDouble(a[1]), 
				Double.parseDouble(a[2]), 
				Double.parseDouble(a[3]));
		return location;
	}

}
