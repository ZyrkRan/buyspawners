package me.zyrkran.buyspawners.utils;

import me.zyrkran.buyspawners.Main;

public class MessageManager {
	
	private Config langConfig;
	
	public MessageManager(Main plugin){
		langConfig = new Config(plugin, "lang_US.yml");
		langConfig.saveDefaultConfig();
		
		reload();
	}
	
	public void reload(){
		
		Messages.NO_PERMISSION 						= getMessage("no-permission");
		Messages.NO_PERMISSION_TO_BREAK_SPAWNER 	= getMessage("no-permission-to-break-spawner");
		Messages.NO_PERMISSION_TO_PLACE_SPAWNER		= getMessage("no-permission-to-place-spawner");
		Messages.MUST_BE_INGAME_PLAYER 				= getMessage("must-be-ingame-player");
		Messages.CONFIG_RELOADED					= getMessage("config-reloaded");
		
		Messages.PLAYER_CHANGED_SPAWNER_TYPE		= getMessage("player-changed-spawner-type");
		Messages.PLAYER_RECEIVED_SPAWNER 			= getMessage("player-received-spawner");
		Messages.PLAYER_PURCHASED_SPAWNER			= getMessage("player-purchased-spawner");
		Messages.PLAYER_PLACED_SPAWNER				= getMessage("player-placed-spawner");
		Messages.PLAYER_DESTROYED_SPAWNER 			= getMessage("player-destroyed-spawner");
		Messages.PLAYER_OPENED_GUI 					= getMessage("player-opened-gui");
		
		Messages.INVALID_PRICE 						= getMessage("invalid-price");
		Messages.INVALID_PRICE_SYMBOL 				= getMessage("invalid-price-symbol");
		Messages.INVALID_SPAWNER_TYPE				= getMessage("invalid-spawner-type");
		Messages.NOT_HOLDING_A_MOB_SPAWNER			= getMessage("not-holding-a-mob-spawner");
		Messages.NOT_LOOKING_AT_MOB_SPAWNER			= getMessage("not-looking-at-mob-spawner");
		
	}
	
	private String getMessage(String msg_id){
		return langConfig.getConfig().getString(msg_id);
	}

}
