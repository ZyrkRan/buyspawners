package me.zyrkran.buyspawners.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import me.zyrkran.buyspawners.Main;
import me.zyrkran.buyspawners.gui.InventoryGUI;
import me.zyrkran.buyspawners.utils.Messages;

public class CommandSpawners implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (!(sender instanceof Player)){
			sender.sendMessage(Main.color(Messages.MUST_BE_INGAME_PLAYER));
			return false;
		}
		
		Player player = (Player) sender;
		
		if (args.length == 0){
			if (player.hasPermission("buyspawners.cmd.spawners")){
				player.openInventory(InventoryGUI.gui());
				player.sendMessage(Main.color(Messages.PLAYER_OPENED_GUI));
				return true;
			}
			
			else {
				sender.sendMessage(Main.color(Messages.NO_PERMISSION));
				return false;
			}
		}
		
		else if (args.length == 1 && args[0].equalsIgnoreCase("reload")){
			if (player.hasPermission("buyspawners.reload")){
				Main.getInstance().reloadConfig();
				Main.getMessageManager().reload();
				
				player.sendMessage(Messages.CONFIG_RELOADED);
				return true;
			}
			
			else{
				player.sendMessage(Messages.NO_PERMISSION);
				return false;
			}
		}
		
		return false;
	}

}
