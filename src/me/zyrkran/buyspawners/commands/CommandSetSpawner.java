package me.zyrkran.buyspawners.commands;

import java.util.Arrays;
import java.util.Set;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.CreatureSpawner;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import me.zyrkran.buyspawners.Main;
import me.zyrkran.buyspawners.spawners.Spawner;
import me.zyrkran.buyspawners.spawners.SpawnerManager;
import me.zyrkran.buyspawners.utils.Messages;
import net.md_5.bungee.api.ChatColor;

public class CommandSetSpawner implements CommandExecutor {
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (!(sender instanceof Player)){
			sender.sendMessage(Main.color(Messages.NO_PERMISSION));
			return false;
		}
		
		Player player = (Player) sender;
		
		// command: /setspawner <block|hand> <type>
		if (args.length == 2){
			if (!player.hasPermission("buyspawners.cmd.setspawner")){
				player.sendMessage(Main.color(Messages.NO_PERMISSION));
				return false;
			}
			
			Spawner spawner = SpawnerManager.getSpawnerByName(args[1]);
			
			if (spawner == null){
				player.sendMessage(Main.color(Messages.INVALID_SPAWNER_TYPE));
				return false;
			}
			
			if (args[0].equalsIgnoreCase("hand")){
				ItemStack itemInHand = player.getInventory().getItemInMainHand();
				if (itemInHand.getType().equals(Material.MOB_SPAWNER)){
					updateSpawnerType(itemInHand, spawner);
					player.sendMessage(Main.color(Messages.PLAYER_CHANGED_SPAWNER_TYPE.replaceAll("%spawner_type%", spawner.getEntityType() + "")));
					return true;
				}
				
				else {
					player.sendMessage(Main.color(Messages.NOT_HOLDING_A_MOB_SPAWNER));
					return false;
				}
			}
			
			else if (args[0].equalsIgnoreCase("block")){
				Block targetBlock = player.getTargetBlock((Set<Material>)null, 5);
				if (targetBlock.getType().equals(Material.MOB_SPAWNER)){
					CreatureSpawner cs = (CreatureSpawner) targetBlock.getState();
					
					cs.setSpawnedType(spawner.getEntityType());
					cs.update(true);
					player.sendMessage(Main.color(Messages.PLAYER_CHANGED_SPAWNER_TYPE.replaceAll("%spawner_type%", spawner.getEntityType() + "")));
					return true;
				}
				
				else {
					player.sendMessage(Main.color(Messages.NOT_LOOKING_AT_MOB_SPAWNER));
					return false;
				}
			}
		}
		player.sendMessage(ChatColor.RED + "Usage: /setspawner <hand|block> <type>");
		return false;
	}
	
	private void updateSpawnerType(ItemStack item, Spawner spawner){
		ItemMeta meta = item.getItemMeta();
		
		meta.setDisplayName(spawner.getName());
		meta.setLore(Arrays.asList("Type: " + spawner.getEntityType()));
		item.setItemMeta(meta);
	}

}
