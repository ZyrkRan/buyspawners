package me.zyrkran.buyspawners.commands;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import me.zyrkran.buyspawners.Main;
import me.zyrkran.buyspawners.utils.Messages;
import net.md_5.bungee.api.ChatColor;

public class CommandBuySpawners implements CommandExecutor {
	
	static List<String> guide = new ArrayList<String>();
	
	static {
		guide.add("BuySpawners v1 - Help Guide");
		guide.add("/spawners - Open spawner selector");
		guide.add("/spawners list - List available spawners");
		guide.add("/setspawners - Change spawners type");
		guide.add("/givespawner - Give spawner to players");
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (!(sender instanceof Player)){
			sender.sendMessage(Main.color(Messages.MUST_BE_INGAME_PLAYER));
			return false;
		}
		
		if (args.length == 0 || args[0].equalsIgnoreCase("help") || args[0].equalsIgnoreCase("?")){
			if (!sender.hasPermission("buyspawners.cmd.buyspawners")){
				sender.sendMessage(Main.color(Messages.NO_PERMISSION));
				return false;
			}
			
			for (String str : guide){
				sender.sendMessage(ChatColor.AQUA + str);
			}
		}
		
		return false;
	}

}
