package me.zyrkran.buyspawners.commands;

import java.util.List;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import me.zyrkran.buyspawners.Main;
import me.zyrkran.buyspawners.spawners.Spawner;
import me.zyrkran.buyspawners.spawners.SpawnerManager;
import me.zyrkran.buyspawners.utils.Messages;
import net.md_5.bungee.api.ChatColor;

public class CommandSpawnerList implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (!(sender.hasPermission("buyspawners.cmd.spawnerlist"))){
			sender.sendMessage(Main.color(Messages.NO_PERMISSION));
			return false;
		}
		
		// send player a list of warps
		StringBuilder sb = new StringBuilder();
		List<Spawner> spawners = SpawnerManager.getLoadedSpawners();
		
		sb.append(ChatColor.DARK_GREEN + "Available spawners (" + spawners.size() + "): " + ChatColor.YELLOW);
		
		if (SpawnerManager.getLoadedSpawners().isEmpty()){
			sb.append("none");
			sender.sendMessage(sb.toString());
			return false;
		}
		
		// append warp names 
		for (Spawner spawner : spawners){
			sb.append(spawner.getName() + ", ");
		}
		
		// Remove the last comma
		sb.deleteCharAt(sb.length()-2);
		
		// Send list of warps to the player
		sender.sendMessage(sb.toString());
		return false;
	}

}
