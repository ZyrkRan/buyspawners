package me.zyrkran.buyspawners.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import me.zyrkran.buyspawners.Main;
import me.zyrkran.buyspawners.spawners.Spawner;
import me.zyrkran.buyspawners.spawners.SpawnerManager;
import me.zyrkran.buyspawners.utils.Messages;
import net.md_5.bungee.api.ChatColor;

public class CommandGiveSpawner implements CommandExecutor {
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		// command: /givespawner <player> <spawner> <amount>
		if (!sender.hasPermission("buyspawners.cmd.givespawner")){
			sender.sendMessage(Main.color(Messages.NO_PERMISSION));
			return false;
		}
		
		if (args.length == 3){
			
			Player target = Bukkit.getServer().getPlayer(args[0]);
			Spawner spawner = SpawnerManager.getSpawnerByName(args[1]);
			
			if (target == null){
				sender.sendMessage(ChatColor.RED + "Error: Player not found!");
				return false;
			}
			
			else if (spawner == null){
				sender.sendMessage(ChatColor.RED + "Error: Invalid spawner type!");
				return false;
			}
			
			else if (!isValidNumber(args[2])){
				sender.sendMessage(ChatColor.RED + "Error: Invalid amount!");
				return false;
			}
			
			else {
				int amount = Integer.parseInt(args[2]);
				
				target.getInventory().addItem(spawner.getItemStack(amount));
				target.sendMessage(ChatColor.DARK_GREEN + "Received " + ChatColor.YELLOW + amount + " " + args[1] + " spawner" + ChatColor.DARK_GREEN + "!");
				return true;
			}
		}
		
		sender.sendMessage(ChatColor.RED + "Usage: /givespawner <player> <spawner> <amount>");
		return false;
	}
	
	private boolean isValidNumber(String string){
		try {
			Integer.parseInt(string);
		} catch (Exception e) {
			return false;
		}
		return true;
	}

}
