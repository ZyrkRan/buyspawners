package me.zyrkran.buyspawners.listeners;

import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

import me.zyrkran.buyspawners.Main;
import me.zyrkran.buyspawners.events.SpawnerBreakEvent;
import me.zyrkran.buyspawners.events.SpawnerPlaceEvent;
import me.zyrkran.buyspawners.events.SpawnerPurchaseEvent;
import me.zyrkran.buyspawners.events.SpawnerSignPurchaseEvent;
import me.zyrkran.buyspawners.spawners.LocationManager;
import me.zyrkran.buyspawners.spawners.Spawner;
import me.zyrkran.buyspawners.spawners.SpawnerManager;
import me.zyrkran.buyspawners.utils.LocationUtils;
import me.zyrkran.buyspawners.utils.Messages;

public class SpawnerListener implements Listener{
	
	LocationManager manager;
	
	public SpawnerListener(Main plugin){
		manager = new LocationManager(plugin);
	}
	
	@EventHandler
	public void onSignSpawnerPurchase(SpawnerSignPurchaseEvent event){
		OfflinePlayer player = Bukkit.getOfflinePlayer(event.getPlayer().getUniqueId());
		Spawner spawner = event.getSpawner();
		
		if (((Player)player).hasPermission("buyspawners.pricebypass")){
			((Player)player).getInventory().addItem(spawner.getItemStack(false));
			//((Player)player).sendMessage(ChatColor.DARK_GREEN + "Received a " + ChatColor.YELLOW + spawner.getAliases().get(0) + ChatColor.DARK_GREEN + " spawner!");
			((Player)player).sendMessage(Main.color((Messages.PLAYER_RECEIVED_SPAWNER.replaceAll("%spawner_type%", spawner.getAliases().get(0)))));
			return;
		}
		
		if (Main.getEconomy().has(player, spawner.getPrice())){
			Main.getEconomy().withdrawPlayer(player, spawner.getPrice());
			//((Player)player).sendMessage(ChatColor.DARK_GREEN + "You purchased a " + ChatColor.YELLOW + spawner.getAliases().get(0) + ChatColor.DARK_GREEN + " spawner for " + ChatColor.YELLOW + "$" + spawner.getPrice());
			((Player)player).sendMessage(Main.color((Messages.PLAYER_PURCHASED_SPAWNER.replaceAll("%spawner_type%", spawner.getAliases().get(0)).replaceAll("%spawner_price%", spawner.getPrice() + ""))));
			((Player)player).getInventory().addItem(spawner.getItemStack(false));
			return;
		}
		
		else {
			((Player)player).sendMessage(Main.color(Messages.INSUFFICIENT_MONEY));
		}
	}
	
	@EventHandler
	public void onSpawnerPurchase(SpawnerPurchaseEvent event){
		OfflinePlayer player = Bukkit.getOfflinePlayer(event.getPlayer().getUniqueId());
		Spawner spawner = event.getSpawner();
		
		if (((Player)player).hasPermission("buyspawners.pricebypass")){
			((Player)player).getInventory().addItem(spawner.getItemStack(false));
			//((Player)player).sendMessage(ChatColor.DARK_GREEN + "Received a " + ChatColor.YELLOW + spawner.getAliases().get(0) + ChatColor.DARK_GREEN + " spawner!");
			((Player)player).sendMessage(Main.color((Messages.PLAYER_RECEIVED_SPAWNER.replaceAll("%spawner_type%", spawner.getAliases().get(0)))));
			return;
		}
		
		if (Main.getEconomy().has(player, spawner.getPrice())){
			Main.getEconomy().withdrawPlayer(player, spawner.getPrice());
			//((Player)player).sendMessage(ChatColor.DARK_GREEN + "You purchased a " + ChatColor.YELLOW + spawner.getAliases().get(0) + ChatColor.DARK_GREEN + " spawner for " + ChatColor.YELLOW + "$" + spawner.getPrice());
			((Player)player).sendMessage(Main.color((Messages.PLAYER_PURCHASED_SPAWNER.replaceAll("%spawner_type%", spawner.getAliases().get(0)).replaceAll("%spawner_price%", spawner.getPrice() + ""))));
			((Player)player).getInventory().addItem(spawner.getItemStack(false));
			return;
		}
		
		else {
			((Player)player).sendMessage(Main.color(Messages.INSUFFICIENT_MONEY));
		}
	}
	
	
	@EventHandler
	public void onSpawnerBreak(SpawnerBreakEvent event){
		Player player = event.getPlayer();
		
		// check for permissions to break spawners
		if (!player.hasPermission("buyspawners.break." + event.getSpawnerType())){
			event.setCancelled(true);
			player.sendMessage(Main.color(Messages.NO_PERMISSION_TO_BREAK_SPAWNER));
			return;
		}
		
		// check if player used Silk touch 
		for (Enchantment en : player.getInventory().getItemInMainHand().getEnchantments().keySet()){
			if (player.getInventory().getItemInMainHand().getEnchantments().containsKey(en)){
				event.getSpawnerBlock().getBlock().setType(Material.AIR);
				event.getSpawnerBlock().getWorld().dropItem(event.getSpawnerLocation(), SpawnerManager.getSpawnerByName(event.getSpawnerType()).getItemStack());
			}
		}
		
		// manage event
		for (String str : manager.getSpawnerLocations()){
			Location a = LocationUtils.locationFromString(str);
			
			if (event.getSpawnerLocation().equals(a)){
				rem(a);
				//player.sendMessage(Main.PREFIX + ChatColor.YELLOW + "A " + event.getSpawnerType() + " spawner was destroyed!");
				player.sendMessage(Main.color((Messages.PLAYER_DESTROYED_SPAWNER.replaceAll("%spawner_type%", event.getSpawnerType()))));
			}
		}
	}
	
	@EventHandler
	public void onSpawnerPlace(SpawnerPlaceEvent event){
		Player player = event.getPlayer();
		
		// check if player has permission to place spawner
		if (!player.hasPermission("buyspawners.place." + event.getSpawnerType())){
			event.setCancelled(true);
			player.sendMessage(Main.color(Messages.NO_PERMISSION_TO_PLACE_SPAWNER));
			return;
		}
		
		// save data to config
		save(event.getSpawnerLocation());
		
		//player.sendMessage(Main.PREFIX + ChatColor.YELLOW + "A " + event.getSpawnerType() + " spawner has been placed!");
		player.sendMessage(Main.color((Messages.PLAYER_PLACED_SPAWNER.replaceAll("%spawner_type%", event.getSpawnerType()))));
	}
	
	private void save(Location location){
		String str = LocationUtils.stringFromLocation(location);
		
		List<String> currentList = manager.getConfig().getConfig().getStringList("locations");
		currentList.add(str);
		
		manager.add(str);
		manager.getConfig().getConfig().set("locations", currentList);
		manager.getConfig().saveConfig();
	}

	private void rem(Location location){
		String str = LocationUtils.stringFromLocation(location);
		
		List<String> currentList = manager.getConfig().getConfig().getStringList("locations");
		currentList.remove(str);
		
		//manager.remove(str);
		manager.getConfig().getConfig().set("locations", currentList);
		manager.getConfig().saveConfig();
	}
}
