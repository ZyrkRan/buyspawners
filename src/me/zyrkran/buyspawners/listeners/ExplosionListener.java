package me.zyrkran.buyspawners.listeners;

import java.util.List;

import org.bukkit.block.Block;
import org.bukkit.block.CreatureSpawner;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityExplodeEvent;

import me.zyrkran.buyspawners.spawners.Spawner;
import me.zyrkran.buyspawners.spawners.SpawnerManager;

public class ExplosionListener implements Listener{
	
	@EventHandler
	public void onExplosion(EntityExplodeEvent event){
		if (event.getEntity() == null){
			return;
		}
		
		List<Block> blocks = event.blockList();
		
		for (Block block : blocks){
			if (block.getState() instanceof CreatureSpawner){
				Spawner spawner = SpawnerManager.getSpawnerByName(((CreatureSpawner)block).getSpawnedType().name());
				
				block.getWorld().dropItemNaturally(block.getLocation(), spawner.getItemStack());
			}
		}
	}
}
