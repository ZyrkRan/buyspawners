package me.zyrkran.buyspawners.listeners;

import org.apache.commons.lang.math.NumberUtils;
import org.apache.commons.lang3.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.block.Sign;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.SignChangeEvent;
import org.bukkit.event.player.PlayerInteractEvent;

import me.zyrkran.buyspawners.Main;
import me.zyrkran.buyspawners.events.SpawnerSignPurchaseEvent;
import me.zyrkran.buyspawners.spawners.Spawner;
import me.zyrkran.buyspawners.spawners.SpawnerManager;
import me.zyrkran.buyspawners.utils.Messages;
import net.md_5.bungee.api.ChatColor;

public class SignListener implements Listener{
	
	@EventHandler
	public void onSignInteract(PlayerInteractEvent event){
		Player player = event.getPlayer();
		
		if (!event.getAction().equals(Action.RIGHT_CLICK_BLOCK)){
			return;
		}
		
		if (event.getClickedBlock().getState() instanceof Sign){
			Sign sign = (Sign) event.getClickedBlock().getState();
			
			if (Main.decolor(sign.getLine(0)).contains("[BuySpawners]")){
				
				Spawner spawner = SpawnerManager.getSpawnerByName(sign.getLine(1));
				if (spawner == null){
					player.sendMessage(Main.color(Messages.INVALID_SPAWNER_TYPE));
					return;
				}
				
				int price = Integer.parseInt(StringUtils.remove(sign.getLine(3), "$"));
				spawner.setPrice(price);
				
				Bukkit.getPluginManager().callEvent(new SpawnerSignPurchaseEvent(player, spawner));
			}
				
		}
	}
	
	@EventHandler
	public void onSignChange(SignChangeEvent event){
		Player player = event.getPlayer();
		String[] lines = event.getLines();
		
		if (lines[0].equalsIgnoreCase("[buyspawners]")){
			if (!player.hasPermission("buyspawners.sign.create")){
				player.sendMessage(Main.color(Messages.NO_PERMISSION));
				event.getBlock().breakNaturally();
				return;				
			}
			
			Spawner spawner = SpawnerManager.getSpawnerByName(event.getLine(1));
			if (spawner == null || lines[1].isEmpty()){
				player.sendMessage(Main.color(Messages.INVALID_SPAWNER_TYPE));
				event.getBlock().breakNaturally();
				return;
			}
			
			String name = lines[1];
			
			if (lines[3].isEmpty()){
				event.setLine(0, ChatColor.DARK_BLUE + "[BuySpawners]");
				event.setLine(1, name);
				event.setLine(2, "");
				event.setLine(3, "$" + spawner.getPrice());
				return;
			}
			
			if (lines[3].startsWith("$")){
				player.sendMessage(Main.color(Messages.INVALID_PRICE_SYMBOL));
				event.getBlock().breakNaturally();
				return;
			}
				
			if (!NumberUtils.isNumber(event.getLine(3))){
				player.sendMessage(Main.color(Messages.INVALID_PRICE));
				event.getBlock().breakNaturally();
				return;
			} 
			
			int price = Integer.parseInt(event.getLine(3));
			event.setLine(0, ChatColor.DARK_BLUE + "[BuySpawners]");
			event.setLine(1, name);
			event.setLine(2, "");
			event.setLine(3, "$" + price);
		}
	}

}
