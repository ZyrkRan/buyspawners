package me.zyrkran.buyspawners.listeners;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.block.CreatureSpawner;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryType.SlotType;
import org.bukkit.inventory.ItemStack;

import me.zyrkran.buyspawners.Main;
import me.zyrkran.buyspawners.events.SpawnerBreakEvent;
import me.zyrkran.buyspawners.events.SpawnerPlaceEvent;
import me.zyrkran.buyspawners.events.SpawnerPurchaseEvent;
import me.zyrkran.buyspawners.spawners.SpawnerManager;

public class PlayerListener implements Listener{
	
	@EventHandler
	public void onInventoryClick(InventoryClickEvent event){
		if (event.getClickedInventory() == event.getWhoClicked().getInventory()){
			event.setCancelled(false);
			return;
		}
		
		// check if player clicked outside of the spawners gui shop
		if (event.getSlotType() == SlotType.OUTSIDE){
			return;
		}
		
		// check if player clicked an actual item
		if (event.getCurrentItem().getType() == Material.AIR 
				|| event.getCurrentItem() == null){
			return;
		}
		
		if (event.getInventory().getTitle().equalsIgnoreCase("Spawners")){
			event.setCancelled(true);
			
			Player player = (Player)event.getWhoClicked();
			ItemStack item = event.getCurrentItem();
			String spawnerType = SpawnerManager.getSpawnerType(item, false);
			
			if (item.getType().equals(Material.MOB_SPAWNER)){
				Bukkit.getPluginManager().callEvent(new SpawnerPurchaseEvent(player, SpawnerManager.getSpawnerByName(spawnerType)));
			}
		}
	}
	
	@EventHandler
	public void onBlockBreak(BlockBreakEvent event){
		Player player = event.getPlayer();
		boolean isMonsterSpawner = event.getBlock().getState() instanceof CreatureSpawner ? true : false;
		
		if (isMonsterSpawner){
			CreatureSpawner cs = (CreatureSpawner) event.getBlock().getState();
			Bukkit.getPluginManager().callEvent(new SpawnerBreakEvent(Main.getInstance(), player, cs));
		}
	}
	
	@EventHandler
	public void onBlockPlace(BlockPlaceEvent event){
		Player player = event.getPlayer();
		boolean isMonsterSpawner = event.getBlockPlaced().getState() instanceof CreatureSpawner ? true : false;

		if (isMonsterSpawner){		
			CreatureSpawner cs = (CreatureSpawner) event.getBlockPlaced().getState();
			ItemStack itemInHand = player.getInventory().getItemInMainHand();
			
			String entityType = SpawnerManager.getSpawnerType(itemInHand, false);

			// set metadata to identify spawner
			if (itemInHand.hasItemMeta()){
				cs.setSpawnedType(EntityType.valueOf(entityType));
			} else {
				cs.setSpawnedType(EntityType.PIG);
			}
			
			// call event
			Bukkit.getPluginManager().callEvent(new SpawnerPlaceEvent(Main.getInstance(), player, cs, itemInHand));
		}
		
	}

}
