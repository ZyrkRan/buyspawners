package me.zyrkran.buyspawners;

import org.bukkit.Bukkit;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

import me.zyrkran.buyspawners.commands.CommandBuySpawners;
import me.zyrkran.buyspawners.commands.CommandGiveSpawner;
import me.zyrkran.buyspawners.commands.CommandSetSpawner;
import me.zyrkran.buyspawners.commands.CommandSpawnerList;
import me.zyrkran.buyspawners.commands.CommandSpawners;
import me.zyrkran.buyspawners.listeners.ExplosionListener;
import me.zyrkran.buyspawners.listeners.PlayerListener;
import me.zyrkran.buyspawners.listeners.SignListener;
import me.zyrkran.buyspawners.listeners.SpawnerListener;
import me.zyrkran.buyspawners.spawners.SpawnerManager;
import me.zyrkran.buyspawners.utils.MessageManager;
import net.md_5.bungee.api.ChatColor;
import net.milkbowl.vault.Vault;
import net.milkbowl.vault.economy.Economy;

public class Main extends JavaPlugin{
	
	public static String PREFIX = ChatColor.DARK_GREEN + "[BuySpawners2] ";
	
	private static Main instance;
	private static SpawnerManager spawnerManager;
	private static MessageManager messageManager;
	
	private Vault vault;
	private static Economy economy;
	
	public void onEnable(){
		instance = this;
		
		// init spawner manager
		spawnerManager = new SpawnerManager(this);
		messageManager = new MessageManager(this);
		
		// register events
		PluginManager pm = Bukkit.getPluginManager();
		pm.registerEvents(new SignListener(), this);
		pm.registerEvents(new PlayerListener(), this);
		pm.registerEvents(new ExplosionListener(), this);
		pm.registerEvents(new SpawnerListener(this), this);
		
		// setup vault & economy
		setupEconomy(pm);
		
		// register commands
		getCommand("spawners").setExecutor(new CommandSpawners());
		getCommand("givespawner").setExecutor(new CommandGiveSpawner());
		getCommand("setspawner").setExecutor(new CommandSetSpawner());
		getCommand("buyspawners").setExecutor(new CommandBuySpawners());
		getCommand("spawnerlist").setExecutor(new CommandSpawnerList());
	}
	
	public static SpawnerManager getSpawnerManager(){
		return spawnerManager;
	}
	
	public static MessageManager getMessageManager(){
		return messageManager;
	}
	
	public static Main getInstance(){
		return instance;
	}
	
	public static Economy getEconomy(){
		return economy;
	}
	
	public static String color(String string){
		return ChatColor.translateAlternateColorCodes('&', string);
	}
	
	public static String decolor(String string){
		return ChatColor.stripColor(string);
	}
	
	private void setupEconomy(PluginManager pm){
		// hook into vault (economy setup)
		vault = (Vault) pm.getPlugin("Vault");
		if (vault != null){
			RegisteredServiceProvider<Economy> rsp = getServer().getServicesManager().getRegistration(Economy.class);
			if (rsp != null){
				economy = rsp.getProvider();
			}
		}
	}
	
}
