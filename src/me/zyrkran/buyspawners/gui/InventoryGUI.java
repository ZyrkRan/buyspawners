package me.zyrkran.buyspawners.gui;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.inventory.Inventory;

import me.zyrkran.buyspawners.spawners.Spawner;
import me.zyrkran.buyspawners.spawners.SpawnerManager;

public class InventoryGUI{

	private static Inventory inventory;
	private static String title;
	
	static {
		title = "Spawners";
		inventory = Bukkit.createInventory(null, 36, ChatColor.translateAlternateColorCodes('&', title));

		for (Spawner spawner : SpawnerManager.getLoadedSpawners()){
			inventory.addItem(spawner.getItemStack());
		}
	}
	
	public static Inventory gui(){
		return inventory;
	}
	
	public static String getTitle(){
		return title;
	}

}
