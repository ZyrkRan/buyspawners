package me.zyrkran.buyspawners.spawners;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.WordUtils;
import org.bukkit.Material;
import org.bukkit.entity.EntityType;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import me.zyrkran.buyspawners.Main;
import me.zyrkran.buyspawners.utils.Config;
import net.md_5.bungee.api.ChatColor;

public class SpawnerManager {
	
	private static List<Spawner> loadedSpawners;
	private Config config;
	
	public SpawnerManager(Main plugin){
		config = new Config(plugin, "mobdata.yml");
		config.saveDefaultConfig();
		
		loadedSpawners = new ArrayList<Spawner>();
		
		// load spawners from mobdata.yml
		for (String s : config.getConfig().getConfigurationSection("mobdata").getKeys(false)){
			EntityType entityType = EntityType.valueOf(s.toUpperCase());
			List<String> aliases = config.getConfig().getStringList("mobdata." + s + ".aliases");
			int price = config.getConfig().getInt("mobdata." + s + ".price");
			int dropChance = config.getConfig().getInt("mobdata." + s + ".drop-chance");
			
			// add spawner to memory
			loadedSpawners.add(new Spawner(s, aliases, entityType, price, dropChance));
		}
	}
	
	public static List<Spawner> getLoadedSpawners(){
		return loadedSpawners;
	}
	
	public static Spawner getSpawnerByName(String string){
		for (Spawner spawner : loadedSpawners){
			if (spawner.getName().equalsIgnoreCase(string)){
				return spawner;
			}
			
			for (String alias : spawner.getAliases()){
				if (alias.equalsIgnoreCase(string)){
					return spawner;
				}
			}
		}
		
		return null;
	}
	
	public static String getSpawnerType(ItemStack item, boolean b){
		return (ChatColor.stripColor(item.getItemMeta().getDisplayName()).replace(' ', '_').toUpperCase()).replaceAll("_SPAWNER", "");
	}
	
	public static String getSpawnerType(ItemStack item){
		ItemMeta meta = item.getItemMeta();
		for (String s : meta.getLore()){
			if (s.contains("Type: ")){
				s = ChatColor.stripColor(s);
				s = StringUtils.remove(s, "Type: ");
				return s.toUpperCase().replace(' ', '_');
			}
		}
		return null;
	}
	
	public static Spawner getSpawnerByType(EntityType type){
		for (Spawner s : loadedSpawners){
			if (s.getEntityType().equals(type)){
				return s;
			}
		}
		return null;
	}
	
	public static ItemStack getSpawnerItemStack(Spawner spawner){
		ItemStack item = new ItemStack(Material.MOB_SPAWNER);
		ItemMeta meta = item.getItemMeta();
		
		meta.setDisplayName(ChatColor.YELLOW + WordUtils.capitalizeFully(spawner.getEntityType().name().replace('_', ' ')) + ChatColor.WHITE + " Spawner");
		meta.setLore(Arrays.asList(ChatColor.YELLOW + "Type: " + WordUtils.capitalizeFully(spawner.getEntityType().name()).replace('_', ' ')));
		meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
		item.setItemMeta(meta);
		
		return item;
	}
	
}
