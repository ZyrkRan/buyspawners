package me.zyrkran.buyspawners.spawners;

import java.util.ArrayList;
import java.util.List;

import me.zyrkran.buyspawners.Main;
import me.zyrkran.buyspawners.utils.Config;

public class LocationManager {
	
	private Config locationCfg;
	private List<String> spawnerLocations;
	
	public LocationManager(Main plugin){
		locationCfg = new Config(plugin, "locations.yml");
		locationCfg.saveDefaultConfig();
		
		spawnerLocations = new ArrayList<String>();
		
		for (String str : locationCfg.getConfig().getStringList("locations")){
			spawnerLocations.add(str);
		}		
	}
	
	public void add(String string){
		if (!spawnerLocations.contains(string)){
			spawnerLocations.add(string);
		}
	}
	
	public void remove(String string){
		if (spawnerLocations.contains(string)){
			spawnerLocations.remove(string);
		}
	}
	
	public Config getConfig(){
		return locationCfg;
	}
	
	public List<String> getSpawnerLocations(){
		return spawnerLocations;
	}

}
