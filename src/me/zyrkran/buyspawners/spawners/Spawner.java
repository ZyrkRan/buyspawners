package me.zyrkran.buyspawners.spawners;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.WordUtils;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.EntityType;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class Spawner {
	
	private String name;
	private String displayName;
	private List<String> aliases;
	private EntityType entityType;
	
	private int price;
	
	public Spawner(String name, List<String> aliases, EntityType entityType, int price, int dropChance){
		this.name = name;
		this.displayName = WordUtils.capitalizeFully(name.replace('_', ' '));
		this.aliases = aliases;
		this.entityType = entityType;
		this.price = price;
	}
	
	public String getName(){
		return name;
	}
	
	public String getDisplayName(){
		return displayName;
	}
	
	public List<String> getAliases(){
		return aliases;
	}
	
	public EntityType getEntityType(){
		return entityType;
	}
	
	public int getPrice(){
		return price; 
	}
	
	public void setPrice(int price){
		this.price = price;
	}
	
	public ItemStack getItemStack(){
		return getItemStack(1, false);
	}
	
	public ItemStack getItemStack(int amount){
		return getItemStack(amount, false);
	}
	
	public ItemStack getItemStack(boolean price){
		return getItemStack(1, price);
	}
	
	public ItemStack getItemStack(int amount, boolean price){
		ItemStack item = new ItemStack(Material.MOB_SPAWNER, amount);
		ItemMeta meta = item.getItemMeta();
		
		meta.setDisplayName(ChatColor.translateAlternateColorCodes('&', "&e" + displayName + " &rSpawner"));
		meta.setLore(price ? 
				//colorLore("&eType: &r" + name, "&ePrice: &r" + price) : colorLore("&eType: &r" + name));
				colorLore("&ePrice: &r" + price) : colorLore(""));
		item.setItemMeta(meta);
		
		return item;
	}
	
	private List<String> colorLore(String... lore){
		List<String> list = new ArrayList<String>();
		
		for (String s : lore){
			list.add(ChatColor.translateAlternateColorCodes('&', s));
		}
		
		return list;
	}

}
