package me.zyrkran.buyspawners.events;

import org.bukkit.Location;
import org.bukkit.block.CreatureSpawner;
import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.bukkit.inventory.ItemStack;

import me.zyrkran.buyspawners.Main;

public class SpawnerPlaceEvent extends Event implements Cancellable {
	
	private Main instance;

	private Player player;
	
	private ItemStack spawnerItem;
	private CreatureSpawner spawnerBlock;

	public SpawnerPlaceEvent(Main instance, Player player, CreatureSpawner spawnerBlock, ItemStack spawnerItem) {
		this.instance = instance;
		this.player = player;
		this.spawnerItem = spawnerItem;
		this.spawnerBlock = spawnerBlock;
	}

	public Player getPlayer() {
		return this.player;
	}
	
	public CreatureSpawner getSpawnerBlock(){
		return spawnerBlock;
	}

	public String getSpawnerType(){
		return spawnerBlock.getCreatureTypeName();
	}
	
	public Location getSpawnerLocation() {
		return spawnerBlock.getLocation();
	}
	
	public String getSpawnerName(){
		return spawnerItem.getItemMeta().getDisplayName();
	}

	public Main getPluginInstance(){
		return instance;
	}

	
	// Spigot crap
	
	private boolean cancel;
	private static HandlerList handlers = new HandlerList();

	@Override
	public boolean isCancelled() {
		return this.cancel;
	}

	@Override
	public void setCancelled(boolean cancel) {
		this.cancel = cancel;
	}

	@Override
	public HandlerList getHandlers() {
		return handlers;
	}

	public static HandlerList getHandlerList() {
		return handlers;
	}
}