package me.zyrkran.buyspawners.events;

import org.bukkit.Location;
import org.bukkit.block.CreatureSpawner;
import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

import me.zyrkran.buyspawners.Main;

public class SpawnerBreakEvent extends Event implements Cancellable {
	
	private Main instance;
	
	private Player player;
	
	private CreatureSpawner spawnerBlock;

	public SpawnerBreakEvent(Main instance, Player player, CreatureSpawner spawnerBlock){
		this.instance = instance;
		this.player = player;
		this.spawnerBlock = spawnerBlock;
	}

	public Player getPlayer(){
		return this.player;
	}
	
	public String getSpawnerType(){
		return spawnerBlock.getCreatureTypeName();
	}
	
	public CreatureSpawner getSpawnerBlock(){
		return spawnerBlock;
	}

	public Location getSpawnerLocation(){
		return spawnerBlock.getLocation();
	}

	public Main getPluginInstance(){
		return instance;
	}
	
	// Spigot crap
	
	private boolean cancel;
	private static HandlerList handlers = new HandlerList();

	@Override
	public boolean isCancelled(){
		return this.cancel;
	}

	@Override
	public void setCancelled(boolean cancel){
		this.cancel = cancel;
	}

	@Override
	public HandlerList getHandlers(){
		return handlers;
	}

	public static HandlerList getHandlerList(){
		return handlers;
	}
}