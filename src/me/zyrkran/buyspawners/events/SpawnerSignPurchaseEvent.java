package me.zyrkran.buyspawners.events;

import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

import me.zyrkran.buyspawners.spawners.Spawner;

public class SpawnerSignPurchaseEvent extends Event implements Cancellable {
	
	private Player player;
	private Spawner spawner;

	public SpawnerSignPurchaseEvent(Player player, Spawner spawner) {
		this.player = player;
		this.spawner = spawner;
	}

	public Player getPlayer() {
		return this.player;
	}
	
	public Spawner getSpawner(){
		return spawner;
	}
	
	// Spigot crap
	
	private boolean cancel;
	private static HandlerList handlers = new HandlerList();

	@Override
	public boolean isCancelled() {
		return this.cancel;
	}

	@Override
	public void setCancelled(boolean cancel) {
		this.cancel = cancel;
	}

	@Override
	public HandlerList getHandlers() {
		return handlers;
	}

	public static HandlerList getHandlerList() {
		return handlers;
	}
}